// **** Main Func (Supposing)
// **** @Desc Main func for register/getting user
user.register = async (req, res, next) => {
    try {
        const { user, is_registered } = await checkAccount(req.email); 
        
        return is_registered? { user, is_registered } : await newEnrollUserHandler(req.name, req.email);
    } catch (error) { next(error); }
}

// ****************************************************************

// ** @Param email (Must be unique identity to check from db, else will return first element)
// ** @Desc Check user existence in db
const checkAccount = async (email) => {
    try {
        const user = await this.findOne({ email });
        const checkUser = !!user && !!(await Directory.getUser(user, user.id));

        return { user, is_registered: checkUser };
    } catch (error) {  throw error; }
}

// ** @Desc Handling user enrollment if not exist
// ** Return (New registered user)
const newEnrollUserHandler = async (name, email) => {
    try {
        const getCredentials = Enrolment.enrollUser(process.env.ADMIN_USERNAME, process.env.ADMIN_PASSWORD);
        const getKeypairs = GPG.generateKeypair();
        const [ creds, keypair ] = await Promise.all([getCredentials, getKeypairs]);
        const user = new this(createUserObj(name, email, creds, keypair));

        await user.save();
        await Directory.createUser(user);
        return { user, is_registered : true };
    } catch (error) { throw error; }
}

// ***************** Supporting Methods *****************
// **
const createUserObj = (name, email, creds, keypair) => {
    return {
        name: name,
        email: email,
        identity: { type: creds['type'], certificate: creds['certificate'], private_key: creds['privateKey'],
                    public_key: creds['publicKey']},
        keypair: { private_key: keypair.privateKey, public_key: keypair.publicKey }
    };
}